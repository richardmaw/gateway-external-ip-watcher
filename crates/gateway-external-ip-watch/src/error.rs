use std::{net::AddrParseError, num::TryFromIntError};

#[non_exhaustive]
#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("UPnP subscription timeout is too large")]
    UPNPSubscribeTimeoutInvalid(TryFromIntError),
    #[error("UPnP device discover failed")]
    UPNPDeviceDiscoverFailed(rupnp::Error),
    #[error("UPnP GetExternalIPAddress method failed")]
    UPNPGetExternalIPAddressFailed(rupnp::Error),
    #[error("UPnP GetExternalIPAddress response is missing field: {0}")]
    UPNPGetExternalIPAddressResponseFieldAbsent(String),
    #[error("UPnP subscribe failed")]
    UPNPSubscribeFailed(rupnp::Error),
    #[error("UPnP subscription receive failed")]
    UPNPSubscriptionReceiveFailed(rupnp::Error),
    #[error("UPnP subscription renewal failed")]
    UPNPRenewSubscriptionFailed(rupnp::Error),
    #[error("UPnP IGD Property subscription is missing ExternalIPAddress")]
    UPNPIGDPropertyMissing,
    #[error("Parsing external IP address failed")]
    ParseExternalIPAddressFailed(AddrParseError),
}
