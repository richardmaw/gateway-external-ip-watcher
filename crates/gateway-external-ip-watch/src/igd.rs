use std::{net::Ipv4Addr, time::Duration};

use futures::{
    future::try_join,
    prelude::*,
    stream::{select_with_strategy, PollNext},
};
use jstream_ext::JTryStreamExt;
use rupnp::ssdp::{SearchTarget, URN};

const WAN_IP_CONNECTION_1: URN = URN::service("schemas-upnp-org", "WANIPConnection", 1);
const WAN_IP_CONNECTION_2: URN = URN::service("schemas-upnp-org", "WANIPConnection", 2);

pub(crate) async fn discover_external_ip(
    discover_timeout: Duration,
    subscribe_timeout: Duration,
) -> Result<impl Stream<Item = Result<Ipv4Addr, crate::error::Error>>, crate::error::Error> {
    use crate::error::Error::*;
    let subscribe_timeout: u32 = subscribe_timeout
        .as_secs()
        .try_into()
        .map_err(UPNPSubscribeTimeoutInvalid)?;

    // Either WANIPConnection:1 or 2 support the required method.
    // Devices shouldn't advertise support for both but may do,
    // so we must request both and deduplicate.
    let (devices1, devices2) = try_join(
        rupnp::discover(&SearchTarget::URN(WAN_IP_CONNECTION_1), discover_timeout),
        rupnp::discover(&SearchTarget::URN(WAN_IP_CONNECTION_2), discover_timeout),
    )
    .map_err(UPNPDeviceDiscoverFailed)
    .await?;
    let devices = devices1.chain(devices2).try_dedup();
    // Personal note: I had hoped I wouldn't need to box the futures
    // but Service::action creates a future that holds a reference
    // to the Service, so it isn't Unpin and can only be made Unpin with box
    // and TryStreamExt has methods that require Unpin rather than just Pin,
    // so
    Ok(devices
        .filter_map(|res| {
            let res = match res {
                Ok(device) => Some(Ok(device)),
                // SSDP discover may have devices that erroneously claim to serve WANIPConnection
                // but don't serve UPnP, so we filter out 404s.
                Err(rupnp::Error::HttpErrorCode(status)) if status.as_u16() == 404 => None,
                Err(e) => Some(Err(UPNPDeviceDiscoverFailed(e))),
            };
            async move { res }
        })
        .and_then(move |device| async move {
            // NOTE: Some versions of miniupnpd report supporting version 2
            // during SSDP discovery, but then do not include it in the schema,
            // So we must discover with all versions but accept either.
            let service = if let Some(service) = device.find_service(&WAN_IP_CONNECTION_2) {
                service
            } else {
                // TOOD: A badly behaved SSDP device could crash this by advertising via SSDP and not providing services,
                // should filter out
                device
                    .find_service(&WAN_IP_CONNECTION_1)
                    .expect("searched for WANIPConnection, did not find version 1 or 2")
            };

            let args = "";
            let response_res_fut = service
                .action(device.url(), "GetExternalIPAddress", args)
                .map_err(UPNPGetExternalIPAddressFailed);
            let subscribe_res_fut = service
                .subscribe(device.url(), subscribe_timeout)
                .map_err(UPNPSubscribeFailed);
            let (response, (sid, property_changes_stream)) =
                try_join(response_res_fut, subscribe_res_fut).await?;
            let initial_address = response
                .get("NewExternalIPAddress")
                .ok_or_else(|| {
                    UPNPGetExternalIPAddressResponseFieldAbsent("NewExternalIPAddress".to_owned())
                })
                .and_then(|initial_address| {
                    initial_address
                        .parse::<Ipv4Addr>()
                        .map_err(ParseExternalIPAddressFailed)
                })?;
            let service = service.clone();
            let resubscribe = stream::once(async move {
                loop {
                    // NOTE: Apart from rupnp depending on tokio
                    // this is so far the only runtime-specific code
                    // TODO: Thread in some form of shutdown handler that ends this loop
                    tokio::time::sleep(Duration::from_secs((subscribe_timeout / 2).into())).await;
                    // TODO: Handle connection failure because service not running
                    // as device gone
                    service
                        .renew_subscription(device.url(), &sid, subscribe_timeout)
                        .await
                        .map_err(UPNPRenewSubscriptionFailed)?;
                }
            });
            let changed_addresses_stream = property_changes_stream
                .map_err(UPNPSubscriptionReceiveFailed)
                .and_then(|state_vars| async move {
                    state_vars
                        .get("ExternalIPAddress")
                        .ok_or(UPNPIGDPropertyMissing)?
                        .parse::<Ipv4Addr>()
                        .map_err(ParseExternalIPAddressFailed)
                });
            let changed_addresses_stream =
                select_with_strategy(resubscribe, changed_addresses_stream, |_: &mut ()| {
                    PollNext::Left
                });

            let addresses =
                stream::once(async move { Ok(initial_address) }).chain(changed_addresses_stream);
            // TODO: try_scan would be a nice replacement instead of Some(Ok(Some(addr)))
            // and then having to try_filter_map to filter out the identival values
            let deduplicated_addresses = addresses
                .scan(None, |st, addr_res| {
                    future::ready(Some(addr_res.and_then(|addr| {
                        Ok(match st {
                            None => {
                                *st = Some(addr);
                                Some(addr)
                            }
                            Some(old_addr) if old_addr != &addr => {
                                *st = Some(addr);
                                Some(addr)
                            }
                            _ => None,
                        })
                    })))
                })
                .try_filter_map(|opt_addr| future::ready(Ok(opt_addr)));
            Ok(deduplicated_addresses)
        })
        // NOTE: try_flatten_unordered would be ideal but is unreleased
        // so we transpose the Result<Stream<Result<T, E>, E> into Stream<Result<>>
        // by returning the error as the stream.
        .flat_map_unordered(None, |res| match res {
            Ok(stream) => stream.boxed().left_stream(),
            Err(e) => stream::once(async move { Err(e) }.boxed()).right_stream(),
        }))
}
