#[cfg(feature = "builder")]
use std::ffi::OsString;
use std::{net::Ipv4Addr, time::Duration};

use futures::prelude::*;
#[cfg(feature = "builder")]
pub use makeit::Buildable;

#[non_exhaustive]
#[cfg_attr(feature = "builder", derive(makeit::Builder))]
#[cfg_attr(feature = "clap", derive(clap::Parser))]
pub struct Settings {
    #[cfg_attr(feature = "builder", default(Duration::from_secs(3)))]
    #[cfg_attr(feature = "clap", clap(long, default_value = "3s", parse(try_from_str = parse_duration::parse)))]
    igd_discover_timeout: Duration,
    #[cfg_attr(feature = "builder", default(Duration::from_secs(300)))]
    #[cfg_attr(feature = "clap", clap(long, default_value = "5min", parse(try_from_str = parse_duration::parse)))]
    igd_subscribe_timeout: Duration,
}

impl Settings {
    pub async fn run(
        self,
    ) -> Result<
        Box<dyn Stream<Item = Result<Ipv4Addr, crate::error::Error>> + Unpin>,
        crate::error::Error,
    > {
        let stream =
            crate::igd::discover_external_ip(self.igd_discover_timeout, self.igd_subscribe_timeout)
                .await?
                .boxed();
        Ok(Box::new(stream))
    }

    #[cfg(feature = "clap")]
    pub fn parse_args<I, T>(args: I) -> Self
    where
        I: IntoIterator<Item = T>,
        T: Into<OsString> + Clone,
    {
        use clap::Parser;
        Settings::parse_from(args)
    }

    #[cfg(feature = "clap")]
    pub fn try_parse_args<I, T>(args: I) -> Result<Self, Box<dyn std::error::Error + Send + Sync>>
    where
        I: IntoIterator<Item = T>,
        T: Into<OsString> + Clone,
    {
        use clap::Parser;
        let settings = Settings::try_parse_from(args)?;
        Ok(settings)
    }
}
