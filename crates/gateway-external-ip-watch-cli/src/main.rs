use futures::prelude::*;

use gateway_external_ip_watch::Settings;

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    //use core::time::Duration;
    //use gateway_external_ip_watch::Buildable;
    //let mut addresses = Settings::builder()
    //    .set_igd_discover_timeout(Duration::from_secs(10))
    //    .build()
    //    .run()
    //    .await?;
    // TODO: Handle parse_args error better
    let settings = Settings::parse_args(std::env::args_os());
    let mut addresses = settings.run().await?;

    while let Some(addr) = addresses.try_next().await? {
        println!("{}", addr)
    }
    Ok(())
}
