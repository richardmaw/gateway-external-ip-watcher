gateway-external-ip-watch
=========================

A project to provide tooling to react to external IP address changes
in a more prompt manner than a cronjob.

It functions by making an initial request for what the current address is
and listens for changes (e.g. SSDP property change for UPnP-IGD).

While it is most common to only have a single gateway on your network
the interface is designed to support having multiple.

It provides 3 APIs:

1.  `gateway-external-ip-watch` command which outputs newline (by default)
    delimited and space (by default) separated IP addresses.

    A line is produced each time an external address changes
    and the line lists all external IP addresses.

    This may be used to send DynDNS updates with:

    ```
    gateway-external-ip-watch | while read ip; do
        ddns-update $domain $ip
    done
    ```
    
    If there's multiple gateway devices on the network
    but only one is significant, then it can be filtered by interface
    and address.

    ```
    gateway-external-ip-watch -i eth0 -a 192.168.0.1 | while read ip; do
        ddns-update $domain $ip
    done
    ```

    There is no perfect solution for _which_ address to bind a domain to.
    A heuristic of "which responds fastest" would be best if all external links
    are equally fast and cheap to use, and all connected to the same internet.
    The kernel routing table may be consulted to defer the heuristic decision,
    though it doesn't help if different gateways have different internet views.
    For a more reliable answer, consult configured DNS servers
    asking whether they serve that domain
    and use the public address of the gateway that the DNS server
    is reachable via.
    Frankly, DynDNS that defaults to "the address that connected to me"
    is the most convenient default behaviour,
    but may receive spurious updates if other gateways update.

2.  `gateway_external_ip_watch::builder` high-level API
    that builds a Stream that can be polled for changes.

3.  `gateway_external_ip_watch::ssdp` low-level APIs
    that expose implementation details,
    should any listener state be convenient to reuse.

It is tested by creating a virtual network containing virtual interfaces
that miniupnpd manage.
Address changes are simulated by using netlink commands to change the address
and then sending a signal to the miniupnpd.
A complete test suite includes:

1.  The common case of a single gateway on the network connected to the client.
    Demonstrating that the new address is reported when it's changed.
2.  A network with multiple gateways,
    connected to the client via different interfaces.
    Demonstrating that when either gateway changes all are listed,
    and that they may be filtered by interface.
3.  A network with multiple gateways connected by the same interface
    but with different addresses.
    Demonstrating that when either gateway changes all are listed,
    and that they may be filtered by address.
