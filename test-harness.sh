#!/bin/sh
# Hacky test script for setting up a test environment.
# Fixed names for global resources are used, and there's no error handling
# or resource cleanup.

# Create test internet router
ip netns add internet
ip netns exec internet sysctl -w net.ipv4.ip_forward=1
ip netns exec internet nft -- add table nat
ip netns exec internet nft -- add chain nat prerouting { type nat hook prerouting priority -100 \; }
ip netns exec internet nft -- add chain nat postrouting { type nat hook postrouting priority 100 \; }

# Create gateway router that connects to the internet
ip netns add gateway
ip netns exec gateway sysctl -w net.ipv4.ip_forward=1
ip netns exec gateway nft -- add table nat
ip netns exec gateway nft -- add chain nat prerouting { type nat hook prerouting priority -100 \; }
ip netns exec gateway nft -- add chain nat postrouting { type nat hook postrouting priority 100 \; }

# Connect gateway to the internet
ip -n internet link add vext-internet type veth peer name vext-gateway
ip -n internet link set vext-gateway netns gateway
ip -n internet address add 1.0.0.1/24 dev vext-internet
ip -n internet link set vext-internet up
ip netns exec internet nft -- add rule nat postrouting oifname vext-internet masquerade
ip -n gateway address add 1.0.0.2/24 dev vext-gateway
ip -n gateway link set vext-gateway up
ip -n gateway route add default via 1.0.0.1 dev vext-gateway
ip netns exec gateway nft -- add rule nat postrouting oifname vext-gateway masquerade

# Add a client device connected to the gateway
ip netns add client
ip -n gateway link add vint-gateway type veth peer name vint-client
ip -n gateway address add 192.168.255.1/24 dev vint-gateway
ip -n gateway link set vint-client netns client
ip -n client address add 192.168.255.2/24 dev vint-client
ip -n gateway link set vint-gateway up
ip -n client link set vint-client up
ip -n client route add default via 192.168.255.1 dev vint-client

ip netns exec gateway miniupnpd -i vext-gateway -a vint-gateway -P miniupnpd.pid
